# frozen_string_literal: true

json.extract! video, :id, :title, :url, :stoptime, :created_at, :updated_at

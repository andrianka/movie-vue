document.addEventListener('turbolinks:load', function(event){

  formFileInput = document.querySelector('.form-file-input')
  if (formFileInput !== null) {
    formFileInput.addEventListener('change',function(e){
      var fileName = document.getElementById("cardFile").files[0].name;
      var nextSibling = e.target.nextElementSibling
      nextSibling.innerText = fileName
    })
  }

  // flash messages remove after 3 sec
  flash_alert = document.getElementById("myAlert");
  if (flash_alert !== null) {
    setTimeout(function(){
      flash_alert.remove('show');
    }, 3000);
  }


  flipCard = function(e){
    // e.classList.add('is-flipped');
    setTimeout(function(){
      var cards_flipped = document.getElementsByClassName('is-flipped');
      if (cards_flipped !== null){
        [].forEach.call(cards_flipped, function(el){
          el.classList.remove('is-flipped');
        })
      }
    });
  };

  window.updateCardGame = function(card_id, event){
    // flipCard(event)
    let game_id = document.URL.split('/').pop()
        prev_card_id = document.getElementById('prev_card');
    let data_params = "game[card_game_id]=" + card_id;
    Rails.ajax({
      type: 'PUT',
      dataType: 'script',
      url: '/games/'+ game_id + '/update_card',
      data: data_params,
      success: function(data,status,xhr) {},
      error: function(xhr,status,error){}
    });
  }
});

# frozen_string_literal: true

class PagesController < ApplicationController
  def home
    @video = Video.new
  end
end

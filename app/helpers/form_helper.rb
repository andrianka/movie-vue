# frozen_string_literal: true

module FormHelper
  def setup_video(video)
    video.subtitle ||= Subtitle.new if video
    video
  end
end

# frozen_string_literal: true

class Video < ApplicationRecord
  has_one :subtitle
  accepts_nested_attributes_for :subtitle, allow_destroy: true

  delegate :upload_file, to: :subtitle, prefix: true
end

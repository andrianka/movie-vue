# frozen_string_literal: true

class Subtitle < ApplicationRecord
  belongs_to :video

  has_one_attached :upload_file
end

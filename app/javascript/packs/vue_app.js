import TurbolinksAdapter from 'vue-turbolinks';
import Vue from 'vue/dist/vue.esm'
import axios from 'axios'
// import VideoPlayer from './components/video_player'


Vue.use(TurbolinksAdapter)

document.addEventListener('turbolinks:load', () => {
  var element = document.getElementById('vue-content');
  var token = document.querySelector('meta[name="csrf-token"]').content;
  if(element != null){

    const app = new Vue({
      el: '#vue-content',
      data: {
        videos_list: [],
        videos: [],
        videoElement: null,
        paused: null,
        url: null,
        url_title: null,
        subtitle: null,
      },
      methods: {
        changeVideofromSidebar(url){
          var video = document.getElementById('videoElement');
          video.src = url;
        },
        changeVideofromModal(url){
          this.closeModal('modalAllVideos');
          this.closeNav();
          var video = document.getElementById('videoElement');
          console.log(url);
          video.src = url;
        },
        loadSubtitle(event){
          this.subtitle = event.target.files[0]
        },
        addVideo(){
          this.videos_list.push({
            title: this.url_title,
            url: this.url
          });
        },
        removeVideo(index){
          this.videos_list.splice(index, 1)
        },
        updatePaused(event) {
          this.videoElement = event.target;
          this.paused = event.target.paused;
        },
        play() {
          this.videoElement.play();
        },
        pause() {
          this.videoElement.pause();
        },
        // header menu
        openNav() {
          document.getElementById("headerNav").style.height = "100%";
        },

        closeNav() {
          document.getElementById("headerNav").style.height = "0%";
        },

        openSidebar() {
          document.getElementById("sidePanel").style.width = "100%";
        },

        closeSidebar() {
          document.getElementById("sidePanel").style.width = "0%";
        },

        openModal(modal_id) {
          document.getElementById(modal_id).style.display='block';
        },

        closeModal(modal_id){
          document.getElementById(modal_id).style.display='none';
        },
        deleteVideo(videoId){
          var url = '/videos/' + videoId;
          axios.delete(url, {
            headers: {
              "X-CSRF-Token": token
            }
          })
          .then(
            document.getElementById('videoId-' + videoId).remove()
          )
          .catch(err => err)
        }

      },
      mounted(){
        if(localStorage.getItem('videos_list')) this.videos_list = JSON.parse(localStorage.getItem('videos_list'));
        axios.get('/videos.json')
                      .then(response => (
                        this.videos = response.data )
                      )
      },

      watch: {
        videos_list:{
          handler(){
            localStorage.setItem('videos_list', JSON.stringify(this.videos_list));
          },
          deep: true,
        }
      },
      computed: {
        playing() { return !this.paused; }
      }
    })
  }
})

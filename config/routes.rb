# frozen_string_literal: true

Rails.application.routes.draw do
  resources :videos
  root 'pages#home'
end

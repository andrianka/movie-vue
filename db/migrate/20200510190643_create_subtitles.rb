# frozen_string_literal: true

class CreateSubtitles < ActiveRecord::Migration[6.0]
  def change
    create_table :subtitles do |t|
      t.references :video, null: false, foreign_key: true

      t.timestamps
    end
  end
end

# frozen_string_literal: true

class RemoveForeignKey < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :subtitles, :videos
  end
end
